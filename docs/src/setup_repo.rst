Setup pipeline repository
=========================

**1. Create a new repo on gitlab that will host your CI-CD pipeline**
   
**2. Navigate to settings -> CI-CD**
    
  |IMG1|

**3. Go to Pipeline triggers add a name on description and click Add Trigger**
    
  |IMG2|

**4. Copy the created trigger token**
    
  |IMG3|
    
**5. Navigate to the original project you want to run the pipeline for, and go to settings -> Webhooks**
    
  |IMG4|

**6. On Webhooks insert the url created from the trigger token**

  - Should be something like ``https://gitlab.com/api/v4/projects/32606458/ref/main/trigger/pipeline?token=6a86fb****99f``
  - ``token`` is the token you just copy from pipeline project
  - ``32606458`` is the project pipeline ID
  - ``main`` is the main branch setup on the pipeline project 
  - |IMG5|
  - You can select which branch changes will trigger this Webhook, in this case only develop branch will

**7. Click on Add Webhooks and that's it**

.. |IMG1| image:: _static/img/setup_step1.png
    :width: 358 px
    :height: 516 px

.. |IMG2| image:: _static/img/setup_step2.png
    :width: 518 px
    :height: 170 px

.. |IMG3| image:: _static/img/setup_step3.png
    :width: 577 px
    :height: 101 px

.. |IMG4| image:: _static/img/setup_step4.png
    :width: 383 px
    :height: 521 px
    
.. |IMG5| image:: _static/img/setup_step5.png
    :width: 532 px
    :height: 306 px