
Introduction
=============

With Taranta you can use your web browser to:

* View a list of all Tango devices
* View and modify device properties
* View and execute device commands
* Create dashboards for interacting with Tango devices.

