Taranta Suite Publish-Subscribe Mechanism
*****************************************
Introduction
============

Publish/Subscribe is a software design pattern that describes the flow of messages between applications, devices, or services in terms of a publisher-to-subscriber relationship.

publish/subscribe mechanism works like this: a publisher (i.e. any source of data) pushes messages out to interested subscribers (i.e. receivers of data) via channels . All subscribers to a specific publisher channel are immediately notified when new messages have been published on that channel, and the message data (or payload) is received together with the notification.

In the Taranta suite, TangoGQL works as publisher and Taranta User Interface  works as subscriber.

When subscription to an attribute is made, the Taranta suite establishes the type of polling mechanism that attribute uses. With this knowledge the Taranta suite determines and selects the most appropriate way to interact with Tango. If Tango events are set up for that attribute, they will be used. However if they aren't the Taranta suite reverts back to using the original mechanism of TangoGQL polling the attribute then publishing updates to the Taranta user interface when it changes.


.\ |IMG0|\

.. |IMG0| image:: _static/img/pub-sub-model-taranta.png
   :height: 312 px
   :width: 771 px

Enable/disable publish-subscribe feature from TangoGQL
=======================================================

TangoGQL has a function called features toggle capable of controlling
some features such as publish-subscribe. There is a file inside tangogql/ called
tangogql.ini, the file looks like this:

.. code-block:: console

  # this configuration file is used to hold details of which features
  # currently enabled in TangoGQL ( True = enabled False = disabled)

  [feature_flags]
  # Publish Subscribe is currently enabled for SKA
  publish_subscribe = True


Changing the `publish_subscribe = True` will enabled pub/sub on TangoGQL,
in this case, TangoGQL will try to Subscribe to changeEvents on the device,
if it fails it tries PeriodicEvents, and if that fails it falls back to
polling

Comparison
==========
In order to demonstrate how the pub/sub can be used to allow different device attributes to be presented at different periodicity, the same process should be repeated for the device Tarantatestdevice. The Tango device Tarantatestdevice was created to allow the pub/sub mechanism to be demonstrated.  It facilitates this by allowing a greater ability to configure polling and event periodicity that what can be achieved with the tg_test device. The tg_test device is limited to only changing its value every second - so even if polling is set to more frequently you won't see any difference, hence Tarantatestdevice was written which does not have this restriction.

* Tango Device: test/Tarantatestdevice/1
* Attribute: RandomAttr

.\ |IMG9|\

.. |IMG9| image:: _static/img/pubsub_9.png
   :height: 130 px
   :width: 438 px

**Figure 9. Screenshot to show Tango Jive and the location of the TarantaTestDevice in the sever listing.**

Using Jive go to the Polling icon of “TarantaTestDevice-->test-->TarantaTestDevice-->test/Tarantatestdevice/1”. For attribute RandomAttr, set the polling period to 500(ms) on the Attribute tab.  Ensure that the polled option is ticked.


.\ |IMG10|\

.. |IMG10| image:: _static/img/pubsub_10.png
   :height: 130 px
   :width: 438 px

**Figure 10. Screenshot to show Tango Jive and Attribute tab in which the Polling characteristics of the selected attribute needs to be activated and an interval be stated.**

For the same Tango Device, select the Event icon. For the RandomAttr attribute set the period to 1000 (ms) on the Periodic event tab. Furthermore, RandomAttr has the Change Event set in order to send events if the current value differs by 1% from the previous value


.\ |IMG11|\

.. |IMG11| image:: _static/img/pubsub_11.png
   :height: 130 px
   :width: 438 px

**Figure 11. Screenshot to show Tango Jive and Attribute tab in which the Event characteristics of the selected attribute needs to be activated and an interval be stated.**

Once the tango devices have been set up in Jive, go back to the Taranta suite and drag a new Attribute Display widget onto the canvas. Set up the Attribute display widget to present the RandomAttr device attribute values in Taranta.


.\ |IMG12|\

.. |IMG12| image:: _static/img/pubsub_12.png
   :height: 130 px
   :width: 438 px

**Figure 12. Screenshot to show Taranta dashboard showing the double_scalar value of tg_test and TarantaTestDevice RandomAttr on seperate attribute display widgets.**

Now run the Taranta suite dashboard by clicking on the Start button.  If set up correctly you should see the two individual device attributes update at different intervals (as defined by the polling and event intervals set via Jive).