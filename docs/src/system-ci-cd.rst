Integrating Standard CI/CD Scripts in SKAO Projects
============================================================

Below is a documentation draft for integrating and using a standardized GitLab CI/CD pipeline setup for 
JavaScript and Python projects within the SKA Telescope organization. This guide focuses on the utilization 
of common scripts and configurations to streamline the testing, linting, and deployment processes across 
multiple projects.

This guide provides instructions for integrating standard CI/CD scripts into JavaScript and Python projects 
at SKAO. These scripts help automate tests, linting, formatting, and Docker builds, ensuring consistency 
and efficiency in development workflows.

Overview
--------

The integration involves using a shared GitLab repository that contains makefile scripts. These scripts are 
designed to be generic enough to be applied across different JavaScript projects within the organization.

Projects Using the Standard CI/CD Setup
---------------------------------------

JavaScript Projects:

|ska-tango-taranta-pipeline|

|ska-tango-taranta-auth-pipeline|

|ska-tango-taranta-dashboard-pipeline|

Python Project:

|ska-tango-tangogql-pipeline|

Pre-requisites
--------------

Before integrating the CI/CD pipeline scripts, ensure the following setup is completed:

Makefile Setup
--------------

For a typical JavaScript project, you need to set environment variables and include JavaScript-specific configurations:

.. code-block:: makefile

    # Define project-specific variables
    PROJECT = ska-tango-taranta
    JS_PROJECT_DIR ?= ./taranta
    JS_PACKAGE_MANAGER ?= npm
    JS_ESLINT_CONFIG ?= .eslintrc.json
    JS_JEST_COMMAND ?= react-scripts test
    JS_JEST_EXTRA_SWITCHES ?=

    # Include JavaScript support
    include .make/js.mk

Pre-build Job Configuration
---------------------------

Modify the project configurations dynamically before the build starts:

.. code-block:: makefile

    oci-pre-build:
	@sed -i "/MIN_WIDGET_SIZE/s/: 20/: $(MIN_WIDGET_SIZE)/" taranta/public/config.js
	@sed -i "/WIDGETS_TO_HIDE/s/: \\[\\]/: $(WIDGETS_TO_HIDE)/" taranta/public/config.js
	@sed -i "/SHOW_COMMAND_FILE_ON_DEVICES/s/: false/: $(SHOW_COMMAND_FILE_ON_DEVICES)/" taranta/public/config.js
	@sed -i "/environment/s/: false/: $(ENVIRONMENT)/" taranta/public/config.js
	@cat taranta/public/config.js


GitLab CI/CD Pipeline Configuration
-----------------------------------

`.gitlab-ci.yml` Setup

Configure the CI/CD pipeline in the `.gitlab-ci.yml` file:

.. code-block:: makefile
    image: $SKA_K8S_TOOLS_BUILD_DEPLOY

    variables:
    GIT_SUBMODULE_STRATEGY: recursive
    CHARTS_TO_PUBLISH: ska-tango-taranta
    JS_PROJECT_DIR: ./taranta
    JS_NODE_VERSION: 18
    UPSTREAM_REPOSITORY: https://gitlab.com/tango-controls/web/taranta.git

    cache:
    paths:
        - ${JS_PROJECT_DIR}

    stages:
    - deploy-stfc
    - clone
    - lint
    - build
    - test
    - scan
    - pages
    - publish

Job Definitions
---------------

Define specific jobs for linting, testing, etc.:

.. code-block:: makefile
    
    js-lint:
    rules:
        - when: always

    js-test:
    variables:
        JS_JEST_EXTRA_SWITCHES: "--runInBand"
    rules:
        - when: always

    include:
        # Javascript
        - project: 'ska-telescope/templates-repository'
            ref: st-1948-support-javascript
            file: 'gitlab-ci/includes/js.gitlab-ci.yml'

Conclusion
----------

By standardizing the CI/CD pipelines across projects, SKAO ensures that all projects maintain high 
code quality and adhere to uniform development practices. 
This setup facilitates easier maintenance and scalability of projects within the organization.


.. |ska-tango-taranta-pipeline| raw:: html

  <a href="https://gitlab.com/ska-telescope/ska-tango-taranta-group/ska-tango-taranta-pipeline" target="_blank">
  ska-tango-taranta-pipeline</a>

.. |ska-tango-taranta-auth-pipeline| raw:: html

  <a href="https://gitlab.com/ska-telescope/ska-tango-taranta-group/ska-tango-taranta-auth-pipeline" target="_blank">
  ska-tango-taranta-auth-pipeline</a>

.. |ska-tango-taranta-dashboard-pipeline| raw:: html

  <a href="https://gitlab.com/ska-telescope/ska-tango-taranta-group/ska-tango-taranta-dashboard-pipeline" target="_blank">
  ska-tango-taranta-dashboard-pipeline</a>

.. |ska-tango-tangogql-pipeline| raw:: html

  <a href="https://gitlab.com/ska-telescope/ska-tango-taranta-group/ska-tango-tangogql-pipeline" target="_blank">
  ska-tango-tangogql-pipeline</a>
