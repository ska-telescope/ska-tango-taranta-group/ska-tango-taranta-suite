Taranta Docker Image Build Process
==================================

Overview
--------
Taranta is an integral part of the Tango-Controls project, collaboratively maintained by |MAXIV| and |SKAO| 
The development process involves a merge request (MR) system, where updates are reviewed and approved by both institutions.

Development and Integration
---------------------------
The source code is hosted on GitLab within the Tango-Controls project:

.. raw:: html

   <a href="https://gitlab.com/tango-controls/web/taranta" target="_blank">Taranta Suite Source Code</a>

When changes are merged into the `develop` branch, they trigger a pipeline in the dedicated SKA repository. 
This repository is responsible for constructing the Docker image, 
configuring it for SKA deployment and pushing it to |Nexus|:

.. raw:: html

   <a href="https://gitlab.com/ska-telescope/ska-tango-taranta-group/ska-tango-taranta-pipeline" target="_blank">
   SKA Tango Taranta Pipeline</a>

Configuration and Deployment
-----------------------------
The SKA repository contains Docker configurations, including `nginx.conf`, 
which is templated to integrate dynamic namespaces during deployments.

A pivotal script, `namespace.sh`, modifies the namespace within configuration files, 
depending of what is set from charts templates using NAMESPACE var,
aligning the Docker image with the targeted deployment environment. The script's functions encompass:

- Writing the namespace to `nginx.html`.
- Modifying static asset paths in the main CSS file.
- Launching the Nginx server in the foreground to enhance performance.

Performance Improvements
------------------------
Transitioning from `npm start` - `Development Build` to `run build` - `Production Build`
has markedly expedited startup times and minimized memory consumption.

|IMG1|

Image size reduction
--------------------

From `~932MB` to `~83MB`

|IMG2|

Image startup time reduction
----------------------------

From `~137s` to `~6s`

|IMG3|

|IMG4|

Script used to check this stats:
--------------------------------


.. code-block:: bash

    #!/bin/bash

    # Set the target pod name, namespace, and URL
    POD_NAME="taranta-ska-tango-taranta-taranta1-0"
    NAMESPACE="testnamespace"
    URL="http://192.168.49.2/testnamespace/taranta/devices"

    # Function to check the web page status
    check_web_page() {
        status_code=$(curl --write-out '%{http_code}' --silent --output /dev/null "$URL")
        if [[ "$status_code" -ge 200 && "$status_code" -lt 300 ]]; then
            return 0
        else
            return 1
        fi
    }

    # Function to reinstall the pod and wait for readiness
    reinstall_pod() {
        start_time=$(date +%s)

        kubectl delete pod $POD_NAME -n $NAMESPACE
        # Pod reinstall logic

        echo -n "Waiting for pod to be running and ready..."
        while true; do
            status=$(kubectl get pod $POD_NAME -n $NAMESPACE -o jsonpath="{.status.phase}")
            ready=$(kubectl get pod $POD_NAME -n $NAMESPACE -o jsonpath="{.status.conditions[?(@.type=='Ready')].status}")

            if [[ $status == "Running" && $ready == "True" ]]; then
                echo -n "Pod is running and ready, checking web page..."
                if check_web_page; then
                    echo "Web page is available."
                    break
                else
                    echo -ne "\rWeb page not ready. Retrying..."
                fi
            else
                echo -ne "\rWaiting for pod to be running and ready..."
            fi
            sleep 1
        done

        end_time=$(date +%s)
        time_taken=$((end_time - start_time))
        echo "Time taken for pod and web page to be ready: $time_taken seconds"
    }

    # Main execution
    reinstall_pod


.. |MAXIV| raw:: html

   <a 
  href="https://www.maxiv.lu.se/" 
  target="_blank">MAXIV</a>

.. |SKAO| raw:: html

   <a 
  href="https://www.skao.int/" 
  target="_blank">SKAO</a>

.. |Nexus| raw:: html

  <a 
  href="https://artefact.skao.int/#browse/browse:helm-internal:ska-tango-taranta" 
  target="_blank">Nexus ska-tango-taranta</a>

.. |IMG1| image:: _static/img/TarantaProduction.png
    :width: 607 px
    :alt: Taranta Image Resource Optimization


.. |IMG2| image:: _static/img/tarantaImageSize.png
    :width: 934 px
    :alt: Taranta Image Size Optimization

.. |IMG3| image:: _static/img/TarantaStartOld.png
    :width: 993 px
    :alt: Taranta Image Size Optimization

.. |IMG4| image:: _static/img/TarantaStartNew.png
    :width: 998 px
    :alt: Taranta Image Size Optimization
