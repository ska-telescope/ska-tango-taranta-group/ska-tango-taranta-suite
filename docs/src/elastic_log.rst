Taranta Elasticsearch log viewer
================================

Taranta has a new widget called Elasticsearch Log Viewer 

Has it stands it makes use of Elastic search on the STFC 

Elastic query URL example -> https://k8s.stfc.skao.int/taranta-namespace/_search 

Kibana UI -> https://k8s.stfc.skao.int/kibana/ 

Connection with STFC: 
---------------------

To connect to the STFC elastic server, taranta uses the direct access IP: http://192.168.99.131:9200, through a service on your namespace
for example https://k8s.stfc.skao.int/taranta-namespace/_search 

If you are using taranta locally on minikube make sure you are connected to the STFC VPN otherwise you won't have access to elastic API and it's results 

The Elastic search Widget: 
--------------------------

The elastic search widget uses the elastic search service to query log stored on the STFC.

This widget has a couple of filters that can be applied in order to filter the logs displayed on the widget

Elastic widget all available filters:
-------------------------------------

+--------------------+--------------------------------------------------------------+
|Filter              |Description                                                   |
+====================+==============================================================+
|Show log as text    |Switches message fields display between table or text         |
+--------------------+--------------------------------------------------------------+
|Search input        |Allows text introduction to filter message field              |
+--------------------+--------------------------------------------------------------+
|From date           |Date/Time introduction to filter FROM message timestamp field |
+--------------------+--------------------------------------------------------------+
|To date             |Date/Time introduction to filter TO message timestamp field   |
+--------------------+--------------------------------------------------------------+
|Multiple Device     |Allows multiple device dropdown selection                     |
+--------------------+--------------------------------------------------------------+
|Log level           |Allows log level dropdown selection                           |
+--------------------+--------------------------------------------------------------+
|Refresh             |Sets the refresh time to query elasticsearch                  |
+--------------------+--------------------------------------------------------------+
|Show overflow scroll|Forces scroll on the widget box                               |
+--------------------+--------------------------------------------------------------+

|IMG1|

Elastic widget device selection filter:
---------------------------------------

|IMG3|

Elastic widget log level selection filter:
------------------------------------------

|IMG4|

Elastic widget refresh time filter:
-----------------------------------

|IMG5|

Elastic date/time selection filter:
-----------------------------------

|IMG6|

Elastic widget type (table or text) of display selection:
---------------------------------------------------------

|IMG7|


Widget RunCanvas
----------------

RunCanvas live filters available:
---------------------------------

|IMG2|

Elastic widget fields as columns:
---------------------------------

|IMG8|

Elastic widget fields as text:
------------------------------

|IMG9|

.. bottom of content

.. |IMG1| image:: _static/img/ElasticInspector.png
   :height: 436 px
   :width: 285 px

.. |IMG2| image:: _static/img/ElasticLiveFilters.png
   :height: 208 px
   :width: 506 px

.. |IMG3| image:: _static/img/ElasticDevice.png
   :height: 377 px
   :width: 263 px

.. |IMG4| image:: _static/img/ElasticLogLevel.png
   :height: 238 px
   :width: 267 px

.. |IMG5| image:: _static/img/ElasticRefresh.png
   :height: 189 px
   :width: 281 px

.. |IMG6| image:: _static/img/ElasticDates.png
   :height: 342 px
   :width: 387 px

.. |IMG7| image:: _static/img/ElasticType.png
   :height: 73 px
   :width: 280 px

.. |IMG8| image:: _static/img/ElasticTable.png
   :height: 287 px
   :width: 720 px

.. |IMG9| image:: _static/img/ElasticText.png
   :height: 298 px
   :width: 715 px
