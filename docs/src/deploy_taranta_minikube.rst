Deploy Taranta on Minikube
==========================

**1. Deploy Minikube on your machine**
    
    - Please follow |deploy_minikube|

**2. Once Minikube is deployed on your machine we will use Taranta Umbrella to deploy Taranta Suite**

    .. code-block:: bash

        git clone --recurse-submodules https://gitlab.com/ska-telescope/ska-tango-taranta-group/ska-tango-taranta-suite.git
        cd ska-tango-taranta-suite
        make k8s-install-chart
    
    Output of make k8s-install-chart

    .. code-block:: bash

        k8s-dep-update: updating dependencies
        +++ Updating ska-tango-taranta-suite chart +++
        Getting updates for unmanaged Helm repositories...
        ...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
        ...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
        ...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
        ...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
        ...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
        ...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
        Saving 6 charts
        Downloading ska-taranta-auth from repo https://artefact.skao.int/repository/helm-internal
        Downloading ska-tango-taranta-dashboard from repo https://artefact.skao.int/repository/helm-internal
        Downloading ska-tango-taranta-dashboard-pvc from repo https://artefact.skao.int/repository/helm-internal
        Downloading ska-tango-taranta from repo https://artefact.skao.int/repository/helm-internal
        Downloading ska-tango-tangogql from repo https://artefact.skao.int/repository/helm-internal
        Downloading ska-tango-examples from repo https://artefact.skao.int/repository/helm-internal
        Deleting outdated charts
        Name:         testnamespace
        Labels:       kubernetes.io/metadata.name=testnamespace
        Annotations:  <none>
        Status:       Active

        No resource quota.

        No LimitRange resource.
        install-chart: install ./charts/ska-tango-taranta-suite/  release: taranta1 in Namespace: testnamespace with params: 
        helm upgrade --install taranta1 \
        \
        ./charts/ska-tango-taranta-suite/  --namespace testnamespace; \
        rm -f gilab_values.yaml
        Release "taranta1" does not exist. Installing it now.
        NAME: taranta1
        LAST DEPLOYED: Thu Feb 24 12:54:17 2022
        NAMESPACE: testnamespace
        STATUS: deployed
        REVISION: 1
        TEST SUITE: None

    This will install the following charts (Taranta+TangoGQL)+Taranta_Auth+Taranta_Dashboard

**3. Now you can run**

    .. code-block:: bash 

        kubectl get -o wide pod -n testnamespace

    And you should see something like:

    .. code-block:: bash 

        NAME                                               READY   STATUS    RESTARTS   AGE     IP            NODE       NOMINATED NODE   READINESS GATES
        dashboard-ska-tango-taranta-dashboard-taranta1-0   1/1     Running   0          3m35s   172.17.0.7    minikube   <none>           <none>
        databaseds-tango-base-test-0                       1/1     Running   0          3m35s   172.17.0.14   minikube   <none>           <none>
        mongodb-ska-tango-taranta-dashboard-taranta1-0     1/1     Running   0          3m35s   172.17.0.13   minikube   <none>           <none>
        ska-tango-base-tangodb-0                           1/1     Running   0          3m35s   172.17.0.9    minikube   <none>           <none>
        tangogql-ska-tango-taranta-taranta1-0              1/1     Running   0          3m35s   172.17.0.12   minikube   <none>           <none>
        tangotest-test-0                                   1/1     Running   0          3m35s   172.17.0.6    minikube   <none>           <none>
        taranta-auth-ska-taranta-auth-taranta1-0           2/2     Running   0          3m35s   172.17.0.10   minikube   <none>           <none>
        taranta-ska-tango-taranta-taranta1-0               1/1     Running   0          3m35s   172.17.0.11   minikube   <none>           <none>

**4. Acess Taranta on browser**

    - Here you should use your ``MINIKUBE_IP`` to acess Taranta on the browser

    - By default the normal IP is 192.168.49.2 and you can go to: |taranta_web|

    - You should see: 
    
        |IMG1|

.. |deploy_minikube| raw:: html

  <a 
  href="https://developer.skao.int/en/latest/getting-started/deploy-skampi.html?highlight=deploy%20minikube#deploy-minikube" 
  target="_blank">SKA Deploy Minikube</a>

.. |taranta_web| raw:: html

  <a 
  href="http://192.168.49.2/testnamespace/taranta/" 
  target="_blank">http://192.168.49.2/testnamespace/taranta/</a>


.. |IMG1| image:: _static/img/Taranta.png
    :width: 600 px
    :height: 460 px