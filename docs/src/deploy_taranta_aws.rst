Deploy Taranta with AWS Integration
===================================

The Taranta suite offers the flexibility to integrate with AWS for authentication and dashboard services. 
This setup allows users to utilize robust, scalable services provided by AWS, 
enhancing the suite's capabilities and reliability.

Configuration Overview
----------------------

To enable AWS integration, two global variables need to be set in the Taranta charts: `use_aws` and `aws_url`.

These variables control the connection to AWS services for authentication and dashboard configurations.

Benefits of AWS Integration
---------------------------

By integrating with AWS, the Taranta suite leverages:

Enhanced scalability and reliability of AWS services.
Robust authentication mechanisms provided by AWS.
Efficient management and storage of dashboard configurations.

**1. Deploy Minikube on your machine**

    - Please follow |deploy_minikube|

**2. To deploy Taranta Suite with AWS Integration for Authentication and Dashboard Services**

    - Update the Taranta suite's Helm chart by setting the global variables to enable AWS integration:

    .. code-block:: yaml

        global:
          use_aws: true
          aws_url: k8s-services.skao.int

    - Clone the Taranta suite repository and deploy using the updated Helm chart:

    .. code-block:: bash

        git clone --recurse-submodules https://gitlab.com/ska-telescope/ska-tango-taranta-group/ska-tango-taranta-suite.git
        cd ska-tango-taranta-suite
        make k8s-install-chart

    - The `make k8s-install-chart` command will deploy the Taranta suite with AWS integration.

**3. Verify the Deployment**

    - Run the following command to check the status of the deployed pods:

    .. code-block:: bash 

        kubectl get -o wide pod -n testnamespace

    |IMG2|

    .. code-block:: bash

        kubectl get -o wide ingress -n testnamespace

    - You should see the external service running with configurations pointing to AWS services.

    |IMG3|

**4. Access Taranta on the browser**

    - Use your ``MINIKUBE_IP`` to access Taranta on the browser.

    - By default, the IP is usually 192.168.49.2. Visit: |taranta_web|

    - You should see the Taranta interface connected to AWS services.

   

.. |deploy_minikube| raw:: html

  <a 
  href="https://developer.skao.int/en/latest/getting-started/deploy-skampi.html?highlight=deploy%20minikube#deploy-minikube" 
  target="_blank">SKA Deploy Minikube</a>

.. |taranta_web| raw:: html

  <a 
  href="http://192.168.49.2/testnamespace/taranta/" 
  target="_blank">http://192.168.49.2/testnamespace/taranta/</a>

.. |IMG1| image:: _static/img/Taranta.png
    :width: 600 px
    :height: 460 px

.. |IMG2| image:: _static/img/TarantaAWS.png
    :width: 941 px

.. |IMG3| image:: _static/img/ingressAWS.png
    :width: 791 px