Deploy Taranta on Docker
==========================

**1. Install Docker on your machine**
    
    - Install Docker if you don’t already have it.

    - To find if Docker is installed on your computer, type in a terminal: ``docker -v`` The output will either say which version of Docker is installed, or that docker is an unknown command.

    - If Docker is not there, point your browser to |get_docker| and follow the instructions for installation.

**2. Install Docker-Compose on your machine**
    
    - Install Docker-Compose if you don’t already have it.

    - To find if Docker is installed on your computer, type in a terminal: ``docker-compose -v`` The output will either say which version of Docker-Compose is installed, or that docker-compose is an unknown command.

    - If Docker-Compose is not there, point your browser to |get_docker_compose| and follow the instructions for installation.
    

**3. Once Docker and Docker-Compose are installed on your machine we will use Taranta Suite project to deploy Taranta Suite**

    .. code-block:: bash

        git clone https://gitlab.com/tango-controls/web/taranta-suite.git
        cd taranta-suite
        make run
    
    This will clone all 4 taranta suite repos to your machine and build them using docker

    .. code-block:: bash

        git clone https://gitlab.com/tango-controls/web/taranta.git
        ...
        git clone https://gitlab.com/tango-controls/web/tangogql.git
        ...
        git clone https://gitlab.com/tango-controls/web/taranta-auth.git
        ...
        git clone https://gitlab.com/tango-controls/web/taranta-dashboard.git
        ...
        Creating taranta-suite_auth_1           ... done
        Creating taranta-suite_traefik_1        ... done
        Creating taranta-suite_mongodb_1        ... done
        Creating taranta-suite_database_1       ... done
        Creating taranta-suite_control-system_1 ... done
        Creating taranta-suite_dashboards_1     ... done
        Creating taranta-suite_tango-test_1     ... done
        Creating taranta-suite_tangogql_1       ... done
        Creating taranta-suite_taranta_1        ... done

 
    After all this your should start seeing projects logs appearing on your console

**4. Acess Taranta on browser**

    - |taranta_docker|

.. |deploy_minikube| raw:: html

  <a 
  href="https://developer.skao.int/en/latest/getting-started/deploy-skampi.html?highlight=deploy%20minikube#deploy-minikube" 
  target="_blank">SKA Deploy Minikube</a>

.. |taranta_web| raw:: html

  <a 
  href="http://192.168.49.2/testnamespace/taranta/" 
  target="_blank">http://192.168.49.2/testnamespace/taranta/</a>

.. |get_docker| raw:: html

  <a 
  href="https://docs.docker.com/get-docker" 
  target="_blank">Get Docker</a>

.. |get_docker_compose| raw:: html

  <a 
  href="https://docs.docker.com/compose/install/" 
  target="_blank">Get Docker-Compose</a>

.. |taranta_docker| raw:: html

  <a 
  href="http://localhost:22484/testdb" 
  target="_blank">http://localhost:22484/testdb</a>


.. |IMG1| image:: _static/img/Taranta.png
    :width: 600 px
    :height: 460 px
    