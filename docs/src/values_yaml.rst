Configuring Taranta with `values.yaml`
======================================

The `values.yaml` file is essential for customizing your Taranta deployment using Helm charts. This guide provides an overview of key configurations in the `values.yaml` file, with a special focus on the `TANGO_DBS` parameter, which allows Taranta to connect to multiple Tango databases.

Understanding `values.yaml`
---------------------------

Below is a sample `values.yaml` file used for deploying Taranta:

.. code-block:: yaml

    # Default values for Taranta.
    # This is a YAML-formatted file.
    # Declare variables to be passed into your templates.

    display: ":0"
    xauthority: "~/.Xauthority"

    basename: true
    TANGO_DBS: []

    logging:
      level: CRITICAL

    global:
      minikube: true
      use_aws: true
      AWS_URL: https://k8s-services.skao.int

    tarantanode:
      enabled: true
      image:
        registry: artefact.skao.int
        image: ska-tango-taranta-pipeline
        tag: 2.11.4
        pullPolicy: IfNotPresent
      resources:
        requests:
          cpu: 300m     # 300m = 0.3 CPU
          memory: 128Mi # 128Mi = 0.125 GB mem
        limits:
          memory: 512Mi # 512Mi = 0.5 GB mem

    nginx:
      worker_processes: 3
      worker_connections: 512

    ingress:
      enabled: true
      nginx: true
      hostname: k8s.stfc.skao.int

      annotations: {}

      tls:
        enabled: true
        secretname: "tls-secret-{{ template \"taranta.name\" . }}-{{ .Release.Name }}"
        hostname: "{{ .Values.ingress.hostname }}"

    nodeSelector: {}

    affinity: {}

    tolerations: []

Key Configuration: `TANGO_DBS`
------------------------------

The `TANGO_DBS` parameter specifies the list of Tango databases that Taranta will interact with. By default, this is an empty list:

.. code-block:: yaml

    TANGO_DBS: []

To connect Taranta to specific Tango databases, populate the `TANGO_DBS` list with the desired database names:

.. code-block:: yaml

    TANGO_DBS: ["taranta2", "taranta3"]

This configuration enables Taranta to interface with multiple Tango databases, allowing for a more flexible and powerful deployment.

Deployment Examples
-------------------

Example 1: Deploying Taranta in `k8s-namespace-1`
-------------------------------------------------

For `k8s-namespace-1`, the deployment includes Taranta connected to multiple Tango databases and multiple instances of the `TangoGQL` chart.

**Taranta Configuration:**

- Set `TANGO_DBS` to `["taranta2", "taranta3"]` in `values.yaml`.

.. code-block:: yaml

    TANGO_DBS: ["taranta2", "taranta3"]

**TangoGQL Deployments:**

1. **TangoGQL1**

   - **HELM_RELEASE**: `tangogql`
   - **tangoDB**: `taranta`
   - **tango_host**: `databaseds-tango-base:10000`

2. **TangoGQL2**

   - **HELM_RELEASE**: `tangogql2`
   - **Database**: Disabled
   - **tangoDB**: `taranta2`
   - **tango_host**: `databaseds-tango-base.testnamespace2.svc.cluster.local:10000`

3. **TangoGQL3**

   - **HELM_RELEASE**: `tangogql3`
   - **Database**: Disabled
   - **tangoDB**: `taranta3`
   - **tango_host**: `databaseds-tango-base.testnamespace3.svc.cluster.local:10000`

Example 2: Alternative Deployment in `k8s-namespace-1`
------------------------------------------------------

In a different scenario within the same namespace, you might connect Taranta to different databases.

**Taranta Configuration:**

- Set `TANGO_DBS` to `["taranta", "taranta2"]` in `values.yaml`.

.. code-block:: yaml

    TANGO_DBS: ["taranta2", "taranta3"]

**TangoGQL Deployments:**

1. **TangoGQL1**

   - **HELM_RELEASE**: `tangogql`
   - **tangoDB**: `taranta`
   - **tango_host**: `databaseds-tango-base:10000`

2. **TangoGQL2**

   - **HELM_RELEASE**: `tangogql2`
   - **tangoDB**: `taranta2`
   - **tango_host**: `databaseds-tango-base.testnamespace2.svc.cluster.local:10000`

Example 3: Deploying Taranta in `k8s-namespace-2`
-------------------------------------------------

For `k8s-namespace-2`, the deployment is straightforward.

**Taranta Configuration:**

- Use default or specify a single Tango database in `TANGO_DBS`.

**TangoGQL Deployment:**

- **HELM_RELEASE**: `tangogql`
- **tangoDB**: `taranta`
- **tango_host**: `databaseds-tango-base:10000`

Configuring Multiple TangoGQL Instances
---------------------------------------

Deploying multiple instances of the `TangoGQL` chart allows Taranta to communicate with different Tango databases across various namespaces. Each instance should have a unique `HELM_RELEASE` name and appropriate configuration.

**Example Configuration for TangoGQL1:**

.. code-block:: yaml

    HELM_RELEASE: tangogql
    tangoDB: taranta
    tango_host: databaseds-tango-base:10000

**Example Configuration for TangoGQL2:**

.. code-block:: yaml

    HELM_RELEASE: tangogql2
    tangoDB: taranta2
    tango_host: databaseds-tango-base.testnamespace2.svc.cluster.local:10000

Important Notes
---------------

- **Database Disabling:** When deploying additional `TangoGQL` instances that connect to external databases, you may need to disable the database creation within the chart by setting the appropriate flags.
  
- **Namespace Awareness:** Ensure that the `tango_host` is correctly specified, especially when connecting to Tango databases in different namespaces.

- **Consistent Configuration:** Maintain consistency between the `TANGO_DBS` list in Taranta and the `tangoDB` specified in each `TangoGQL` instance to ensure proper communication.

Customizing Other `values.yaml` Parameters
------------------------------------------

While `TANGO_DBS` is critical for database connections, other parameters in `values.yaml` allow you to customize your deployment further.

- **Global Settings:**

  .. code-block:: yaml

      global:
        minikube: true
        use_aws: true
        AWS_URL: https://k8s-services.skao.int

  - **minikube:** Set to `true` if deploying on Minikube.
  - **use_aws:** Enable AWS integration by setting to `true`.
  - **AWS_URL:** Specify the AWS service URL if `use_aws` is enabled.

- **Taranta Node Configuration:**

  .. code-block:: yaml

      tarantanode:
        enabled: true
        image:
          registry: artefact.skao.int
          image: ska-tango-taranta-pipeline
          tag: 2.11.4
          pullPolicy: IfNotPresent
        resources:
          requests:
            cpu: 300m
            memory: 128Mi
          limits:
            memory: 512Mi

  Customize the Docker image and resource allocations as needed.

- **Nginx Configuration:**

  .. code-block:: yaml

      nginx:
        worker_processes: 3
        worker_connections: 512

  Adjust Nginx settings for performance tuning.

- **Ingress Configuration:**

  .. code-block:: yaml

      ingress:
        enabled: true
        nginx: true
        hostname: k8s.stfc.skao.int
        annotations: {}
        tls:
          enabled: true
          secretname: "tls-secret-{{ template \"taranta.name\" . }}-{{ .Release.Name }}"
          hostname: "{{ .Values.ingress.hostname }}"

  Configure ingress settings to manage external access to the Taranta application.

Conclusion
----------

By carefully configuring the `values.yaml` file, you can tailor your Taranta deployment to meet the specific requirements of your environment. Whether connecting to multiple Tango databases or adjusting resource allocations, the `values.yaml` provides the flexibility needed for complex deployments.

For more detailed information on each parameter, refer to the comments within the `values.yaml` file or consult the official Taranta documentation.

