Taranta Doc Setup
=================

This section will explain about setting up the documentation for 4 taranta repos. Doc pages for the 4 taranta repos can be found on
:doc:`index`. On this page we will discuss about testing doc changes on local, creating doc, creating build and verifying doc build

Theese steps are similar for all 4 taranta repos. Lets start with the one below:

Testing doc on dev: 
-------------------

To compile and test your doc changes on local dev environment, navigate to respective repo and open terminal and run below command:

    - ``docker run -it --rm -v $(pwd):/tmp -w /tmp/docs netresearch/sphinx-buildbox bash``

    - ``make html``

|IMG1|

Setting up sphinx doc: 
----------------------

To create a sphinx doc:

    - First import gitlab repo in |readthedocs|. This will create an empty doc website.

    - Get webhook address and secret: Navigate to Admin > Integration > Gitlab incoming webhook in readthedocs. This will show **address** and **secret** to configure webhook in gitlab

    - Navigate to repo in gitlab > Settings > Webhook > Add new webhook. Use **address** & **secret** from step 2 to populate URL & secret token here.

      |IMG2|   

    - Select the trigger event (eg. Push events, Merge request events, etc.) which will invoke the doc build on readthedocs. Finally click ``Add webhook`` to add the webhook.

    - After adding webhook, navigate to webhook page, click on ``Test`` button, this will trigger build on readthedocs  


Creating doc for custom branch:
-------------------------------

There are times when we want to deploy our custom (non develop) branch on readthedocs. To do so follow below steps:

- Navigate to ``versions`` tab on projects readthedocs. For eg. **https://readthedocs.org/projects/taranta-auth/versions/** in our case

- Jump to ``Activate a version`` and type the name of branch. For eg. **ct-1110-fix-doc-issue**. Then hit **Filter Inactive Versions** button.

   |IMG3|

- Select your branch and hit ``Activate`` button next to it. This will direct you to select build variables. Click ``Active`` checkbox to make it visible. 

   |IMG4|

- Hit ``Save`` button. This will create a build for your branch, navigate to ``Builds`` menu to see its status.

- After successful build creation, navigate to repos doc page, at the bottom left, click on ``v:latest`` link.

   |IMG5|

- Finally select your branch from the dialog. For eg. **ct-1110-fix-doc-issue** in our case.

   |IMG6|

- After clicking on custom branch, it will redirect you to your custom branch doc. For eg. https://taranta-auth.readthedocs.io/en/ct-1110-fix-doc-issue/ in our case.

   |IMG7|


Contribution & Help:
--------------------

In order to contribute or to get assistance from Taranta Team, please reach out to us on |slack|


.. |readthedocs| raw:: html

  <a 
  href="https://readthedocs.org/dashboard/import" 
  target="_blank">ReadTheDocs</a>

.. |slack| raw:: html

  <a 
  href="https://skao.slack.com/archives/CUPDD1KM5" 
  target="_blank">Taranta Collab Slack</a>


.. bottom of content

.. |IMG1| image:: _static/img/local_env.png
   :height: 52 px
   :width: 1211 px

.. |IMG2| image:: _static/img/webhook.png
   :height: 553 px
   :width: 662 px

.. |IMG3| image:: _static/img/filter_version.png
   :height: 322 px
   :width: 825 px

.. |IMG4| image:: _static/img/create_build.png
   :height: 450 px
   :width: 617 px

.. |IMG5| image:: _static/img/open_build.png
   :height: 316 px
   :width: 307 px

.. |IMG6| image:: _static/img/select_build.png
   :height: 316 px
   :width: 307 px

.. |IMG7| image:: _static/img/custom_doc.png
   :height: 435 px
   :width: 883 px
