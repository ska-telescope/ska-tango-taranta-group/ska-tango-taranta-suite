Deploy taranta suite with multiple DB
=====================================

Overview
--------

In this use case, we demonstrate the capability of the TangoGQL application to interface with multiple TangoDB instances across various Kubernetes namespaces. This feature enables a scalable deployment model that supports different database instances, allowing for isolated or shared database access across services.

User Story
----------

As a developer, I want to deploy multiple instances of TangoGQL in a single Kubernetes namespace, where each instance connects to a distinct TangoDB in different namespaces. This setup should allow each instance to uniquely address and manage its designated TangoDB without interference, facilitating modular and scalable application development.

Scenario: Multiple TangoGQL Instances with Separate Databases
-------------------------------------------------------------

|IMG1|

Context
^^^^^^^

- **k8s-namespace-1**:

  - Hosts three instances of TangoGQL (``TangoGQL1``, ``TangoGQL2``, ``TangoGQL3``).

  - Each instance connects to a different TangoDB instance.

  - Taranta Web application is deployed to interact with all three TangoGQL instances.

- **k8s-namespace-2**:

  - Hosts ``Tango-DB2``, which is connected by ``TangoGQL2`` deployed in ``k8s-namespace-1``.

- **k8s-namespace-3**:

  - Hosts ``Tango-DB3``, which is connected by ``TangoGQL3`` deployed in ``k8s-namespace-1``.

Requirements
^^^^^^^^^^^^

- Each TangoGQL instance must have its unique Helm release name when deployed in the same namespace.
- The ``tango_host`` configuration for each TangoGQL must correctly point to the respective TangoDB service endpoint.

Deployment Details
^^^^^^^^^^^^^^^^^^

1. **Deploy TangoGQL1**
   - ``tangoDB``: taranta1
   - ``tango_host``: databaseds-tango-base:10000 (connecting to Tango-DB1 in ``k8s-namespace-1``)

2. **Deploy TangoGQL2**
   - ``tangoDB``: taranta2
   - ``tango_host``: databaseds-tango-base.k8s-namespace-2.svc.cluster.local:10000 (connecting to Tango-DB2 in ``k8s-namespace-2``)

3. **Deploy TangoGQL3**
   - ``tangoDB``: taranta3
   - ``tango_host``: databaseds-tango-base.k8s-namespace-3.svc.cluster.local:10000 (connecting to Tango-DB3 in ``k8s-namespace-3``)

Usage
^^^^^

- **Taranta Web** in ``k8s-namespace-1`` provides a user interface to select and interact (on widget inspector panel) with all three TangoGQL instances, offering a comprehensive view and control over multiple databases.

|IMG2|

Conclusion
----------

This deployment scenario exemplifies the flexibility and scalability of using TangoGQL with the multiDB feature in a Kubernetes environment, facilitating robust development practices and efficient resource utilization across multiple services.

.. |IMG1| image:: _static/img/MultipleDBCase1.png
    :width: 600 px

.. |IMG2| image:: _static/img/TarantaMultipleDBSelection.png
    :width: 284 px
