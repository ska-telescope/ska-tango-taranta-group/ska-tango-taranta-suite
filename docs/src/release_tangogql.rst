Release a tangoGQL image and chart
==================================

- **1**: Create a new Issue on the |release_management| Jira Project with a summary of your release

- **2**: Choose which bump version you want to use:

    - bump-major-release
    - bump-minor-release
    - bump-patch-release
  
  Run for example ``make bump-patch-release``, if for example .release was ``1.2.1`` it will be moved to ``1.2.2``.

  Update the values.yml if neeeded.

- **3**: Run ``make helm-set-release`` this will set all charts to example ``1.2.2`` version

- **4**: Run ``make python-set-release`` this will set pyproject.toml to example ``1.2.2`` version
 
- **5**: Run ``make git-create-tag``

    - Do you wish to continue (will commit outstanding changes) [N/y]: y
    - Tell me your Jira Ticket ID (REL-999): REL-1234

- **6**: ``make git-push-tag``

  Note: If you are releasing chart for a custom branch for taranta
     a. You will have to stop the pipelines triggered by ``make git-push-tag`` in step 6
     b. Run the above pipelines with your custom branch name in ``BRANCH`` variable.
        |IMG1|
        These pipelines would use your custom branch for creating build and charts.
  

.. |release_management| raw:: html

  <a 
  href="https://jira.skatelescope.org/projects/REL/issues" 
  target="_blank">Release Management</a>


.. bottom of content

.. |IMG1| image:: _static/img/run_pipeline.png
   :height: 501 px
   :width: 604 px
