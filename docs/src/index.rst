.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Home
  :hidden:

SKA Taranta Suite
-----------------

Welcome to the SKA Taranta Suite ReadTheDocs

What follows is a brief guide to the headings you'll find in the left-hand sidebar of this site. Feel free to explore!

Projects that compose Taranta Suite
-----------------------------------

.. list-table:: 
   :widths: 1 1 1 1
   :header-rows: 1

   * - Repository 
     - Docs
     - Purpose
     - Description
   * - https://gitlab.com/tango-controls/web/taranta.git
     - |Taranta_Docs|
     - A tool for creating Dashboards for 
       interacting with the devices 
       within a Tango Control System
     - The tool for developing these dashboards is taranta itself.  

   * - https://gitlab.com/tango-controls/incubator/tangogql-ariadne.git
     - |TangoGQL Ariadne_Docs|
     - Queryable access to a Tango Control System that can be used by the 
       dashboard creation tool
     - Currently, this is a TangoGQL Ariadne. A GraphQL web server that integrates with the 
       TangoDB services and cancommunicate directly with tango devices.

   * - https://gitlab.com/tango-controls/web/taranta-dashboard.git  
     - |Taranta Dashboard_Docs|
     - Storing and Sharing the configuration of developed dashboards
       between users
     - A MongoDB based dashboard repository for storing taranta dashboard   
       layouts

   * - https://gitlab.com/tango-controls/web/taranta-auth.git 
     - |Taranta Auth_Docs|
     - Authorization and Authentication for the tools 
     - A simple authentication and authorization service for the taranta and 
       TangoGQL tools that can be hooked into a corporate       
       authentication solution 

   * - https://gitlab.com/tango-controls/web/taranta-suite.git
     - -
     - Supporting further development
     - A set of developer scripts and tools used for setting up and developing 
       these related products. 

Introduction to Taranta Suite
-----------------------------------------------
.. INTRODUCTION SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Introduction
  :hidden:

  introduction
  pubsub

- :doc:`introduction`
- :doc:`pubsub`

How to Deploy Taranta Suite on Minikube
---------------------------------------

This section describes how to deploy the Taranta Suite in a Minikube environment. 
You now have two options for deploying the suite: 

- Using local services 

- Integrating with AWS for authentication and dashboard services.

Option 1: Local Deployment (Default)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For a standard local deployment without AWS integration:

.. note::
  - Use the latest Charts:

    - Taranta: |ska-tango-taranta|
    - TangoGQL-Ariadne: |ska-tango-tangogql-ariadne|
    - Taranta-Dashboard: |ska-tango-taranta-dashboard|
    - Taranta-Auth: |ska-taranta-auth|

``For an example of creating your own chart, please follow:`` |ska-tango-taranta-suite|

Option 2: AWS Integration (Preferred)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For deploying with AWS integration for authentication and dashboard services:

.. note::
  - Enable AWS Integration by setting global variables in the Taranta suite's Helm chart:

    .. code-block:: yaml

        global:
          use_aws: true
          aws_url: k8s-services.skao.int

  - Follow the steps outlined in `deploy_taranta_aws` below for detailed instructions on deploying Taranta with AWS integration.


.. GETTING STARTED SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Getting Started
  :hidden:

  system-ci-cd
  deploy_taranta_aws
  deploy_taranta_minikube
  deploy_taranta_docker
  taranta_users
  values_yaml

- :doc:`system-ci-cd`
- :doc:`deploy_taranta_aws`
- :doc:`deploy_taranta_minikube`
- :doc:`deploy_taranta_docker`
- :doc:`taranta_users`
- :doc:`values_yaml`

For SKA Developers
------------------

.. DEVELOPERs SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Developers
  :hidden:

  setup_repo
  release_tangogql
  upload_images
  feedback
  elastic_log
  taranta_image
  docs_setup
  deploy_multiple_db

- :doc:`setup_repo`
- :doc:`release_tangogql`
- :doc:`feedback`
- :doc:`elastic_log`
- :doc:`taranta_image`
- :doc:`docs_setup`
- :doc:`deploy_multiple_db`

Joint Process for Contribution between Max IV and SKA
-----------------------------------------------------

.. Contribution MAXIV-SKA SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Contribution MAXIV-SKA
  :hidden:

  contribution

- :doc:`contribution`

.. |ska-tango-taranta| raw:: html

  <a href="https://artefact.skao.int/#browse/browse:helm-internal:ska-tango-taranta%2F2.12.1" target="_blank">ska-tango-taranta:2.12.1</a>

.. |ska-tango-tangogql-ariadne| raw:: html

  <a href="https://artefact.skao.int/#browse/browse:helm-internal:ska-tango-tangogql-ariadne%2F0.1.1" target="_blank">ska-tango-tangogql-ariadne:0.1.1</a>

.. |ska-taranta-auth| raw:: html

  <a href="https://artefact.skao.int/#browse/browse:helm-internal:ska-tango-taranta-auth%2F0.2.3" target="_blank">ska-tango-taranta-auth:0.2.3</a>

.. |ska-tango-taranta-dashboard| raw:: html

  <a href="https://artefact.skao.int/#browse/browse:helm-internal:ska-tango-taranta-dashboard%2F1.6.4" target="_blank">ska-tango-taranta-dashboard:1.6.4</a>

.. |ska-tango-taranta-suite| raw:: html

  <a href="https://gitlab.com/ska-telescope/ska-tango-taranta-group/ska-tango-taranta-suite/-/tree/main/charts/taranta-suite-umbrella" target="_blank">ska-tango-taranta-suite</a>

.. |Taranta| raw:: html

  <a 
  href="https://gitlab.com/tango-controls/web/taranta" 
  target="_blank">Taranta</a>

.. |TangoGQL Ariadne| raw:: html

  <a 
  href="https://gitlab.com/tango-controls/incubator/tangogql-ariadne" 
  target="_blank">TangoGQL Ariadne</a>

.. |Taranta Auth| raw:: html

  <a 
  href="https://gitlab.com/tango-controls/web/taranta-auth" 
  target="_blank">Taranta Auth</a>

.. |Taranta Dashboard| raw:: html

  <a 
  href="https://gitlab.com/tango-controls/web/taranta-dashboard"
  target="_blank">Taranta Dashboard</a>

.. |Taranta_Docs| raw:: html

  <a 
  href="https://taranta.readthedocs.io/en/latest/"
  target="_blank">Taranta Docs</a>

.. |TangoGQL Ariadne_Docs| raw:: html

  <a 
  href="https://developer.skao.int/projects/ska-tango-tangogql-ariadne-pipeline/en/latest/"
  target="_blank">TangoGQL Ariadne Docs</a>

.. |Taranta Auth_Docs| raw:: html

  <a 
  href="https://taranta-auth.readthedocs.io/en/latest/"
  target="_blank">Taranta Auth Docs</a>

.. |Taranta Dashboard_Docs| raw:: html

  <a 
  href="https://taranta-dashboard.readthedocs.io/en/latest/"
  target="_blank">Taranta Dashboard Docs</a>
