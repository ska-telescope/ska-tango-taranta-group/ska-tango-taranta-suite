KUBE_NAMESPACE?=testnamespace
CI_COMMIT_SHA?=local
VERSION?=$(shell cat package.json | grep version | head -1 | awk -F: '{ print $$2 }' | sed 's/[\",]//g' | tr -d '[[:space:]]')
HELM_RELEASE?=taranta1

# Fixed variables
TIMEOUT = 86400

# Docker and Gitlab CI variables
RDEBUG ?= ""
CI_ENVIRONMENT_SLUG ?= development
CI_PIPELINE_ID ?= pipeline$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=8 count=1 2>/dev/null;echo)
CI_JOB_ID ?= job$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=4 count=1 2>/dev/null;echo)
GITLAB_USER ?= ""
CI_BUILD_TOKEN ?= ""
REPOSITORY_TOKEN ?= ""
REGISTRY_TOKEN ?= ""
GITLAB_USER_EMAIL ?= "nobody@example.com"
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
CI_APPLICATION_TAG ?= $(shell git rev-parse --verify --short=8 HEAD)
DOCKERFILE ?= Dockerfile
EXECUTOR ?= docker
STAGE ?= build_react_artefacts

KUBE_HOST ?= http://192.168.49.2/
KUBE_NAMESPACE ?= testnamespace
KUBE_DB ?= taranta

CLUSTER_DOMAIN ?= cluster.local
K8S_CHART_PARAMS = --set global.cluster_domain=$(CLUSTER_DOMAIN)

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#

# include OCI Images support
include .make/oci.mk

# include k8s support
include .make/k8s.mk

# include Helm Chart support
include .make/helm.mk

# Include Python support
include .make/python.mk

# include raw support
include .make/raw.mk

# include core make support
include .make/base.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

watch:
	@watch kubectl get pod -n $(KUBE_NAMESPACE);

cypress-e2e-interactive:
	docker run -v $(shell pwd):/e2e --network=host -w /e2e -it cypress/base:12 /bin/bash

tests:
	apt-get update && \
	apt-get -y install jq && \
	npm ci && \
	$$(npm bin)/cypress cache path && \
	$$(npm bin)/cypress cache list && \
	$$(npm bin)/cypress verify && \
	$$(npm bin)/cypress run

local-tests:
	npm ci && \
	./node_modules/.bin/cypress cache path && \
	./node_modules/.bin/cypress cache list && \
	./node_modules/.bin/cypress verify && \
	./node_modules/.bin/cypress run

install_jq:
	sudo apt-get update && sudo apt-get install jq
	
config_minikube:
	jq -n '{"url":"$(KUBE_HOST)","namespace":"$(KUBE_NAMESPACE)","db":"$(KUBE_DB)"}' > cypress/config.json

readthedocs:
	docker run --rm -d -v $(shell pwd):/tmp -w /tmp/docs netresearch/sphinx-buildbox sh -c "make html"