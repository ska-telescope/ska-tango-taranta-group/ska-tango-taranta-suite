import config from "../config.json";

describe('Frontend Integration test', () => {
    it(' navigate devices', () => {
        cy.viewport(1920, 1080);
        cy.visit(config.url + config.namespace + '/' + config.db)

        //login
        cy.contains('Log In').click();
        cy.get('[type="text"]').type('DEFAULT');
        cy.get('[type="password"]').type('DEFAULT_SKA');
        cy.get('.modal-footer [type="submit"]').click();

        cy.wait(1000);
        cy.contains('sys').click();
        cy.wait(1000);
        cy.contains('tg_test').click();
        cy.wait(1000);
        cy.get('[href="/' + config.namespace + '/' + config.db + '/devices/sys/tg_test/1/server"]').click();
        cy.wait(2000);
        cy.contains('Commands').click();

        //check if che command button is enables if the user is logged
        cy.get('[title="DevVoid"]').parents("tr").children(".input").children("[type=button]").should('not.be.disabled');

        cy.contains('Log Out').click();
        //check if che command button is disabled after the logout
        cy.get('[title="DevVoid"]').parents("tr").children(".input").children("[type=button]").should('be.disabled');

        cy.contains('Dashboards').click();

    });

})
