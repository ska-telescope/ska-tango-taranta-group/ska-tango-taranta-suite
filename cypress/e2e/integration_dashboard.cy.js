import config from "../config.json";
import 'cypress-wait-until';

describe('Integration test dashboard', () => {
    it('create dashboards', () => {
        cy.viewport(1920, 1080);
        cy.visit(config.url + config.namespace + '/' + config.db);
        cy.contains('Log In').click();
        cy.get('[type="text"]').type('DEFAULT').should('have.value', 'DEFAULT');
        cy.get('[type="password"]').type('DEFAULT_SKA').should('have.value', 'DEFAULT_SKA');
        cy.get('.modal-footer [type="submit"]').click();

        cy.contains('Dashboards').click();

        let dashboardNames = []

        for (let i = 0; i < 2; i++) {
            cy.get('.dashboard-add-dashboard').click();

            cy.waitUntil(() => cy.contains('Dashboard created').then(() => {
                cy.get('.closepop').click();
            }), {
                errorMsg: 'Error creating the dashboard',
                timeout: 2000,
                interval: 500
            });

            cy.get('.dashboard-menu [type="text"]').click();
            cy.wait(2000);
            cy.get('.dashboard-menu [type="text"]').clear();
            cy.wait(2000);
            dashboardNames[i] = "dash_" + Math.random().toString(36).replace(/[^a-z]+/g, '');
            cy.get('.dashboard-menu [type="text"]').type(dashboardNames[i].toString() + '{enter}');

            cy.waitUntil(() => cy.get('[title="Delete dashboard \'' + dashboardNames[i] + '\'"]').then(() => {
                cy.get('.Placeholder').click();
            }), {
                errorMsg: 'Error renaming the dashboard',
                timeout: 2000,
                interval: 500
            });
            cy.contains(dashboardNames[i]).should('exist');
        }

        dashboardNames.forEach(dashboardName => {

            cy.waitUntil(() => cy.get('[title="Delete dashboard \'' + dashboardName + '\'"]').click().then(() => {
                cy.get('.btn-primary').click()
            }), {
                errorMsg: 'Error deleting the dashboard',
                timeout: 2000,
                interval: 500
            });

            cy.contains(dashboardName).should('not.exist');
        })

        cy.contains('Log Out').click();
    })
})
