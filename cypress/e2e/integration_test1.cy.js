import config from "../config.json";
import { dash } from "../dashboards_files/test_dashboard"

var now = new Date();
dash.name = "Test_" + now.toLocaleString();
var dash_id = ""

describe('Integration test1', () => {
    it('navigate taranta test and try to login and logout', () => {
        cy.viewport(1920, 1080);
        cy.visit(config.url + config.namespace + '/' + config.db);
        cy.contains('Log In').click();
        cy.get('[type="text"]').type('DEFAULT').should('have.value', 'DEFAULT');
        cy.get('[type="password"]').type('DEFAULT_SKA').should('have.value', 'DEFAULT_SKA');
        cy.get('.modal-footer [type="submit"]').click();
        cy.get('.LogInOut span').contains('DEFAULT');
        cy.contains('Dashboards').click();
        cy.request('POST', config.namespace + '/dashboards/', dash).then(
            (response) => {
                cy.log(response.body);
                dash_id = response.body.id;
                cy.visit(config.url + config.namespace + '/' + config.db + '/dashboard?id=' + dash_id);
                cy.contains('Start').click();
                cy.wait(5000);
                cy.contains('Edit').click();
                cy.contains('Log Out').click();
            }
        )
    })
})