import config from "../config.json";
import { dash } from "../dashboards_files/test_dashboard"

var now = new Date();
dash.name = "Test_" + now.toLocaleString();
var dash_id = ""

describe('Tangogql Test1', () => {
    it('Test attribute value changes with tango', () => {
        cy.viewport(1920, 1080);
        cy.visit(config.url + config.namespace + '/' + config.db);
        cy.contains('Log In').click();
        cy.get('[type="text"]').type('DEFAULT').should('have.value', 'DEFAULT');
        cy.get('[type="password"]').type('DEFAULT_SKA').should('have.value', 'DEFAULT_SKA');
        cy.get('.modal-footer [type="submit"]').click();
        cy.get('.LogInOut span').contains('DEFAULT');
        cy.contains('Dashboards').click();

        cy.request('POST', config.url + config.namespace + '/dashboards/', dash).then(
            (response) => {
                cy.log(response.body);
                dash_id = response.body.id;
                cy.visit(config.url + config.namespace + '/' + config.db + '/dashboard?id=' + dash_id);
                cy.contains('Start').click();
                cy.wait(2000);
                cy.contains('double_scalar_rww: ');

                // Check if values are pulling from tango
                cy.get('#AttributeDisplay')
                    .invoke('text')
                    .then((text1) => {

                        cy.wait(2000);
                        // grab the div again and compare its previous text to the current text
                        cy.get('#AttributeDisplay')
                            .invoke('text')
                            .should((text2) => {
                                expect(text1).not.to.eq(text2)
                            })
                    })

                //Delete given dashboard
                cy.contains('Edit').click();
                cy.wait(5000);
                cy.contains(dash.name).should('not.exist');
                cy.contains('Log Out').click();
            }
        )


    })
})