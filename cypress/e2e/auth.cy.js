import config from "../config.json";

describe('Authentication Test1', () => {
    it('Login test with different users', () => {
        const users = [
            {
                username: 'DEFAULT',
                password: 'DEFAULT_SKA',
            }
        ]

        cy.viewport(1920, 1080);
        cy.visit(config.url + config.namespace + '/' + config.db);

        users.forEach(user => {
            cy.contains('Log In').click();
            cy.get('[type="text"]').type(user.username).should('have.value', user.username);
            cy.get('[type="password"]').type(user.password).should('have.value', user.password);
            cy.get('.modal-footer [type="submit"]').click();
            cy.get('.LogInOut span').contains(user.username);
            cy.wait(3000);
            cy.get('.LogInOut').contains('Log Out').click();
            cy.wait(3000);
        })

        cy.request('POST', config.namespace + '/auth/login', users[0]).then(
            (response) => {
                cy.log(response.body);
                expect(response.status).to.eq(200);
                expect(response.body.taranta_jwt).to.not.eq('');
            }
        )
    })
})