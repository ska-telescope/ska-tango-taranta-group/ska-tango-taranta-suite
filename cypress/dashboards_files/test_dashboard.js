export let dash = {
    "id": "",
    "widgets": [
        {
            "id": "1",
            "x": 84,
            "y": 23,
            "canvas": "0",
            "width": 20,
            "height": 4,
            "type": "ATTRIBUTE_DISPLAY",
            "inputs": {
                "attribute": {
                    "device": "sys/tg_test/1",
                    "attribute": "double_scalar_rww",
                    "label": "double_scalar_rww"
                },
                "precision": 2,
                "showDevice": false,
                "showAttribute": "Label",
                "scientificNotation": false,
                "showEnumLabels": false,
                "showAttrQuality": false,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "font": "Helvetica",
                "widgetCss": ""
            },
            "order": 0
        },
        {
            "id": "2",
            "x": 80,
            "y": 44,
            "canvas": "0",
            "width": 60,
            "height": 40,
            "type": "ATTRIBUTE_PLOT",
            "inputs": {
                "timeWindow": 120,
                "showZeroLine": true,
                "logarithmic": false,
                "attributes": [
                    {
                        "attribute": {
                            "device": "sys/tg_test/1",
                            "attribute": "double_scalar_rww",
                            "label": "double_scalar_rww"
                        },
                        "showAttribute": "Label",
                        "yAxis": "left",
                        "lineColor": "#000000"
                    }
                ],
                "textColor": "#000000",
                "backgroundColor": "#ffffff"
            },
            "order": 1
        }
    ],
    "name": "Dashboard1",
    "variables": [],
    "tangoDB": "testdb", 
    "user": "DEFAULT",
    "insertTime": "2024-11-21T11:15:11.599Z",
	"updateTime": "2024-11-21T11:15:24.132Z"
  }