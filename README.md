# ska-tango-taranta-suite

- Clone this repo using `git clone --recursive https://gitlab.com/ska-telescope/ska-tango-taranta-group/ska-tango-taranta-suite.git`

- Docs available at: https://ska-tango-taranta-suite.readthedocs.io/en/latest/